import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';

export const Team = (props) => {
  const handleDragStart = (e) => e.preventDefault();

  const items = [
    <img src="img/team/01.jpg"  onDragStart={handleDragStart} />,
    <img src="img/team/02.jpg"  onDragStart={handleDragStart} />,
    <img src="img/team/03.jpg"  onDragStart={handleDragStart} />,
    <img src="img/team/04.jpg"  onDragStart={handleDragStart} />,
  ];

  const items2 = [
    <img src="path-to-img" onDragStart={handleDragStart} />,
    <img src="path-to-img" onDragStart={handleDragStart} />,
    <img src="path-to-img" onDragStart={handleDragStart} />,
  ];

  const responsive = {
    0: { items: 1 },
    568: { items: 1 },
    1024: { items: 1 },
  }
  return (
    <div id='team' className='text-center'>
      <div className='container'>
        <div className='col-md-12 col-lg-12 col-xl-12 col-12 section-title'>
          <h2>Выкупим любой автомобиль:</h2>
          <p> несколько примеров</p>
          {/* <p>
            с начала работы мы выкупили более 220 000 автомобилей
          </p> */}
        </div>
        <div className='col-md-12 col-lg-12 col-xl-12 col-12'>
          <AliceCarousel 
            mouseTracking 
            items={items} 
            responsive={responsive}
            animationType="fadeout"
            disableButtonsControls/>
        </div>
        <a
          href='https://auction.carfast.kz/upload-photos'
          className='btn btn-custom btn-lg page-scroll '
        >
          Продать свое авто
        </a>{' '}
      </div>
    </div>
  )
}
