import ReactGa from "react-ga"
export const Header = (props) => {

  const ClickBuyBtn = () => {
    ReactGa.event({
      category: "Button Buy Carfast.kz",
      action: "Нажали Купить на сайте carfast.kz"
    })
  }

  const ClickSellBtn = () => {
    ReactGa.event({
      category: "Button Sell Carfast.kz",
      action: "Нажали Продать на сайте carfast.kz"
    })
  }

  return (
    <header id='header'>
      <div className='intro'>
        <div className='overlay'>
          <div className='container'>
            <div className='d-flex'>
              <div className='col-md-12 intro-text'>
                <h1>
                  {props.data ? props.data.title : 'Loading'}
                  <span></span>
                </h1>
                <div className='d-flex'>
                  <div className='left'>
                    <h3 className='leftTitle'>
                      {props.data ? props.data.titleLeft : 'Loading'}
                      <span></span>
                    </h3>
                    <div>
                      {props.data ? 
                          props.data.paragraph.split("/").map(function(item, idx) {
                            return (
                                <p key={idx}>
                                    {item}
                                    <br/>
                                </p>
                            )
                          }) : 'Loading'}
                    </div>
                    <div className="  ">
                      <a
                        href='https://auction.carfast.kz'
                        className='btn btn-custom btn-lg page-scroll'
                        onClick={ClickBuyBtn}
                      > 
                        Купить
                      </a>{' '}
                    </div>
                  </div>
                  <div className='right'>
                    <h3 className='rightTitle'>
                      {props.data ? props.data.titleRight : 'Loading'}
                      <span></span>
                    </h3>
                    <div>
                    {props.data ? 
                      props.data.paragraph2.split("/").map(function(item, idx) {
                        return (
                            <p key={idx}>
                                {item}
                                <br/>
                            </p>
                        )
                      }) : 'Loading'}
                    </div>
                    <div>
                      <a
                        href='https://auction.carfast.kz/upload-photos'
                        className='btn btn-custom btn-lg page-scroll'
                        onClick={ClickSellBtn}
                      >
                        Продать
                      </a>{' '}
                    </div>
                  </div>
                </div>
                
                <div className='carsImg d-flex space-b '>
                  <img src='img/carLeft.png' className='img-responsive car' alt='' />{' '}
                  <img src='img/car.png' className='img-responsive car' alt='' />{' '}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}
