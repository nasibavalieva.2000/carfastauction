export const TradeIn = (props) => {
    return (
      <div id='tradein' className='text-center'>
        <div className='container d-flex'>
          <div className='col-md-10 cont d-flex'>
            {props.data
            ? <div className='divImg d-flex '><img src={props.data.img} alt='' />{' '}</div>
            : 'Loading...'}
            <div className ='ml-20'>
                <h3>8 из 10 автомобилей</h3>
                <h2>оцениваются выше, чем в Trade-In</h2>
            </div>
          </div>
        </div>
      </div>
    )
  }
  