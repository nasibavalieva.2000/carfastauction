export const Features = (props) => {
  return (
    <div id='features' className='text-center'>
      <div className='container'>
        <div className='col-md-10 col-md-offset-1 section-title'>
          <h2>3 простых шага</h2>
          <p>чтобы продать автомобиль за 1 день</p>
        </div>
        <div className='row'>
          {props.data
            ? props.data.map((d, i) => (
                <div key={`${d.title}-${i}`} className='col-xs-12 col-md-4 d-flex d-column steps'>
                  {' '}
                  {/* <i data-src={d.img} ></i> */}
                  <div className='divImg d-flex '><img src={d.img} alt='' />{' '}</div>
                  <div className='d-flex d-column step'>
                    <h3>{d.title}</h3>
                    <p>{d.text}</p>
                  </div>
                </div>
              ))
            : 'Loading...'}
        </div>
      </div>
    </div>
  )
}
