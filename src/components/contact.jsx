import { useState } from 'react'
import emailjs from 'emailjs-com'

const initialState = {
  name: '',
  email: '',
  message: '',
}
export const Contact = (props) => {
  const [{ name, email, message }, setState] = useState(initialState)

  const handleChange = (e) => {
    const { name, value } = e.target
    setState((prevState) => ({ ...prevState, [name]: value }))
  }
  const clearState = () => setState({ ...initialState })

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(name, email, message)
    emailjs
      .sendForm(
        'YOUR_SERVICE_ID', 'YOUR_TEMPLATE_ID', e.target, 'YOUR_USER_ID'
      )
      .then(
        (result) => {
          console.log(result.text)
          clearState()
        },
        (error) => {
          console.log(error.text)
        }
      )
  }
  return (
    <div>
      <div id='footer'>
        <div className='container text-center'>
          <p className="footer-title">Адрес: <span>Масанчи 98б, офис 113</span></p>
          <p className="footer-title">Телефон: <span><a href="tel:+77008367509">+7 700 836-75-09</a></span></p>
          <p className="footer-title">Тех. поддержка: <span><a href="tel:+77025930920">+7 702 593 0920</a></span></p>
          <p>
            &copy; 2021 Carfast - аукцион автомобилей в Казахстане
          </p>
        </div>
      </div>
    </div>
  )
}
