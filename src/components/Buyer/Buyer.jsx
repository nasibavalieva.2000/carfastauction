import './Buyer.css'
import React,{useState} from 'react';
import { Helmet } from 'react-helmet'

import logo from '../../assets/logo.png'
import car from '../../assets/camry.png'
import auction from '../../assets/buyer/card11.png'
import auctionSto from '../../assets/buyer/card22.png'
import HappyMan from '../../assets/buyer/card33.png'

const Card = (props) =>{
  const { title, content, color, isHidden, toggleIsHidden,isOpen } = props;
  if(isOpen) {
    return (
        <div
          onClick={toggleIsHidden}
          className="card question-card"
          style={{ background: color }}
        >
            <h3 className="question-title">
              {title}
              <div className="question-icon">
                <i class="fa fa-plus" aria-hidden="true"></i>
              </div>
            </h3>
          <p className={isHidden ? 'hidden' : ''}>{content}</p>
        </div>
      );         
    }else{
        return (
            <div
              onClick={toggleIsHidden}
              className="card question-card"
              style={{ background: color }}
            >
                <h3 className="question-title">
                  {title}
                <div className="question-icon">
                  <i class="fa fa-minus" aria-hidden="true"></i>
                </div>
                </h3>
              <p className={isHidden ? 'hidden' : 'question-content'}>{content}</p>
            </div>
          );         
        }
}


export const Buyer = () => {
  const [state,setState] = useState([
    {
      title: 'Сколько это стоит для покупателя?',
      content:
        'Всё абсолютно бесплатно.',
      isHidden: false,
      color: '#F1F1F1'
    },
    {
        title: 'Вы берете комиссию?',
        content:
          'Мы не берем комиссий со сделки.',
        isHidden: false,
        color: '#F1F1F1'
    }
  ]);
  const toggle = card => {
    const cards = state;
    const index = cards.indexOf(card);
    setState([
        ...cards.slice(0, index),
        { ...card, isHidden: !card.isHidden },
        ...cards.slice(index + 1),
      ],
    );
  };
  return ( 
    <div className="container-buyer" style={{'padding-bottom': '30px'}}>
      <Helmet>
        {/* encodeSpecialCharacters={false}
        defer={false} */}
        <title>Carfast.kz для покупателей</title>
        <script type='text/javascript'>{`
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '472661503967783');
          fbq('track', 'PageView');`}
        </script>
        <noscript>
          {` 
            <img height="1" width="1" style="display:none"
              src="https://www.facebook.com/tr?id=472661503967783&ev=PageView&noscript=1"
            />
          `}
        </noscript>
      </Helmet>
      <header className="header">
        <div className="header-wrapper">
          <img className="logo" src={logo} alt="logo" />
          <p>Первый в Казахстане онлайн аукцион автомобилей</p>
        </div>
        <div className="header-content">
          <h1 className="page1-title">
            Покупай и продавай (автомобили) по самой выгодной цене <span>без посредников и без комисий</span>
          </h1>
          <div className="car-image-wrapper">
            <img src={car} alt="car" className="car"></img>
          </div>
        </div>
      </header>
      <section className="page1">
        <ul>
          <li>Более 300 аукционов каждый день</li>
          <li>Оплата после живого просмотра на СТО</li>
          <li>Есть рассрочка</li>
        </ul>
        <a href="https://t.me/carfastchannel" className="buy-button">ХОЧУ ВЫГОДНО КУПИТЬ АВТО</a>

        <a href="https://auction.carfast.kz/upload-photos" className="sale-button">ХОЧУ ПРОДАТЬ АВТО</a>

      </section>
      <section className="page2">
        <h2>КАК ЭТО РАБОТАЕТ?</h2>
        <div className="image-card-wrapper">
          <img src={auction} className="image-card" alt="image" />
          <p>Смотрите на аукционе интересующие
          авто и ставите цену за которую готовы
          купить авто </p>
        </div>
        <div className="image-card-wrapper">
          <img src={auctionSto} className="image-card" alt="image" />
          <p>Продавец соглашается на Вашу цену.
          CarFast.kz помогает оформить
          и гарантирует безопасную сделку </p>
        </div>
        <div className="image-card-wrapper">
          <img src={HappyMan} className="image-card" alt="image" />
          <p>CarFast.kz гарантирует безопасную сделку.
          В случае покупки задаток вычитается
          из стоимости авто. При отказе от покупки,
          задаток возвращается  </p>
        </div>
        <a href="https://t.me/carfastchannel" class="buy-button">ХОЧУ ВЫГОДНО КУПИТЬ АВТО</a>
      </section>

      <section class="page3">
        <h2>ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ</h2>
        <div id='questions'>
          <div className='question-wrapper'>
            {state.map(card => {
                return (
                    <Card
                    toggleIsHidden={() => toggle(card)}
                    color={card.color}
                    title={card.title}
                    isHidden={card.isHidden}
                    content={card.content}
                    isOpen = {card.isHidden}
                    />
                );
            })}
          </div>
        </div>

        <a href="https://t.me/carfastchannel" class="buy-button" style={{ 'margin-top': '40px' }}>ХОЧУ ВЫГОДНО КУПИТЬ АВТО</a>

        <a href="https://auction.carfast.kz/upload-photos" class="sale-button">ХОЧУ ПРОДАТЬ АВТО</a>
      </section>

    </div>
  )
}