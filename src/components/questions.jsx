import React,{useState} from 'react';

const Card = (props) =>{
  const { title, content, color, isHidden, toggleIsHidden,isOpen } = props;
  if(isOpen) {
    return (
        <div
          onClick={toggleIsHidden}
          className="card"
          style={{ background: color }}
        >
            <h3>{title}
                <i class="fa fa-plus" aria-hidden="true"></i>
            </h3>
          <p className={isHidden ? 'hidden' : ''}>{content}</p>
        </div>
      );         
    }else{
        return (
            <div
              onClick={toggleIsHidden}
              className="card"
              style={{ background: color }}
            >
                <h3>{title}
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </h3>
              <p className={isHidden ? 'hidden' : ''}>{content}</p>
            </div>
          );         
        }
}

export const Questions = (props) => {
    const [state,setState] = useState([
        {
          title: 'Какие автомобили вы покупаете?',
          content:
            'На площадке CarFast продаются и покупаются абсолютно любые авто. Марка, год выпуска, пробег, состояние значения не имеют.',
          isHidden: true,
        },
        {
          title: 'Сколько это стоит для продавца?',
          content:
          'Всё абсолютно бесплатно.',
          isHidden: true,
        },
        {
          title: 'Сколько это стоит для покупателя?',
          content:
            'Всё абсолютно бесплатно.',
          isHidden: true,
        },
        {
            title: 'Вы берете комиссию?',
            content:
              'Мы не берем комиссий со сделки.',
            isHidden: true,
        },
        {
            title: 'Я могу отказаться от продажи?',
            content:
                'Да, конечно. Если вас не устроит последняя цена в аукционе, которую предложат покупатели, Вы можете просто отказаться от продажи. Вы в любом случае не обязаны ни за что платить. А если решите вернуться, мы будем только рады!',
            isHidden: true,
        },
        {
            title: 'Я хочу продать максимально дорого!',
            content:
                'Чтобы увеличить возможную цену на 5-10%, перед публикацией своего авто помойте машину и сделайте фотографии максимально качественно так, как указано у нас в примерах, а также снимите объявление с других площадок по продаже авто. Оценка через аукцион приводит к увеличению цены за автомобиль. В 80% случаев оценка выше, чем получится в трейд-ин.',
            isHidden: true,
        },
        {
            title: 'Как вы оцениваете машину?',
            content:
                'Сделанные вами качественные фотографии автомобиля просматриваются дилерами, автосалонами и профессиональными покупателями которые делают свои ставки и за 25-30 минут определяется максимальная цена, за которую готовы выкупить автомобиль здесь и сейчас.',
            isHidden: true,
        },
        {
            title: 'Так кому я продаю машину?',
            content:
                'Финальными приобретателями автомобиля являются автосалоны, дилеры и другие профессионалы рынка автомобилей.',
            isHidden: true,
        },
        {
            title: 'Можно оставить вам автомобиль на продажу?',
            content:
                'Да. Вы можете оставить свой  автомобиль  нашим партнерам, которые его продадут.',
            isHidden: true,
        },
      ]);
    const toggle = card => {
        const cards = state;
        const index = cards.indexOf(card);
        setState([
            ...cards.slice(0, index),
            { ...card, isHidden: !card.isHidden },
            ...cards.slice(index + 1),
          ],
        );
      };
    return (
      <div id='questions'>
        <div className='container cont'>
            <div className='col-md-10 col-md-offset-1 section-title text-center'>
                <h2>Остались вопросы?</h2>
                <p>Мы с удовольствием ответим</p>
            </div>
                
                {/* <h3>Why Choose Us?</h3> */}
          <div className='row col-xs-12 col-md-12 d-flex'>
            <div className='col-xs-12 col-md-10 d-flex d-column'>
                {state.map(card => {
                    return (
                        <Card
                        toggleIsHidden={() => toggle(card)}
                        color={card.color}
                        title={card.title}
                        isHidden={card.isHidden}
                        content={card.content}
                        isOpen = {card.isHidden}
                        />
                    );
                })}
            </div>
          </div>
        </div>
      </div>
    )
  }