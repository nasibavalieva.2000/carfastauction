export const Partners = (props) => {
  return (
    <div id="partners" className="text-center">
      <div className="container partner-container">
        <div className="col-md-10 col-md-offset-1 section-title">
          <h2>Наши партнеры</h2>
        </div>
        <div className="partners-wrapper">
          { props.data ? props.data.map((item) => (
              <div className="partner-item">
                <img className="partner-item__logo" src={item.img} alt={item.name} />
                <span>{ item.name }</span>
              </div>
          )) : 'Loading ...' }
        </div>

      </div>
    </div>
  )
}