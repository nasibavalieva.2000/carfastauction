import { useState, useEffect } from 'react'
import JsonData from '../data/data.json'
import {Navigation} from './navigation'
import {TradeIn} from './tradein'

import SmoothScroll from 'smooth-scroll'
export const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 1000,
  speedAsDuration: true,
})
export const Main = () => {
  const [landingPageData, setLandingPageData] = useState({})
  useEffect(() => {
    setLandingPageData(JsonData)
  }, [])
  return (
    <div>
      <Navigation />
      <TradeIn data={landingPageData.TradeIn}/>
    </div>
  )
}