import './Seller.css'
import React,{useState} from 'react';
import { Helmet } from 'react-helmet'
import { useParams } from 'react-router-dom'

import logo from '../../assets/logo.png'
import car from '../../assets/camry.png'
import auctionScreen1 from '../../assets/seller/card-seller11.png'
import auctionScreen2 from '../../assets/seller/card-seller22.png'
import auctionScreen3 from '../../assets/seller/card-seller33.png'


const Card = (props) =>{
  const { title, content, color, isHidden, toggleIsHidden,isOpen } = props;
  if(isOpen) {
    return (
        <div
          onClick={toggleIsHidden}
          className="card question-card"
          style={{ background: color }}
        >
            <h3 className="question-title">
              {title}
              <div className="question-icon">
                <i class="fa fa-plus" aria-hidden="true"></i>
              </div>
            </h3>
          <p className={isHidden ? 'hidden' : ''}>{content}</p>
        </div>
      );         
    }else{
        return (
            <div
              onClick={toggleIsHidden}
              className="card question-card"
              style={{ background: color }}
            >
                <h3 className="question-title">
                  {title}
                <div className="question-icon">
                  <i class="fa fa-minus" aria-hidden="true"></i>
                </div>
                </h3>
              <p className={isHidden ? 'hidden' : 'question-content'}>{content}</p>
            </div>
          );         
        }
}

export const Seller = () => {
  const [state,setState] = useState([
    {
      title: 'Какие автомобили вы покупаете?',
      content:
        'На площадке CarFast продаются и покупаются абсолютно любые авто. Марка, год выпуска, пробег, состояние значения не имеют.',
      isHidden: false,
      color: '#F1F1F1'
    },
    {
      title: 'Сколько это стоит для продавца?',
      content:
        'Всё абсолютно бесплатно.',
      isHidden: false,
      color: '#F1F1F1'
    },
    {
        title: 'Я могу отказаться от продажи?',
        content:
            'Да, конечно. Если вас не устроит последняя цена в аукционе, которую предложат покупатели, Вы можете просто отказаться от продажи. Вы в любом случае не обязаны ни за что платить. А если решите вернуться, мы будем только рады!',
        isHidden: false,
        color: '#F1F1F1'
    },
    {
        title: 'Я хочу продать максимально дорого!',
        content:
            'Чтобы увеличить возможную цену на 5-10%, перед публикацией своего авто помойте машину и сделайте фотографии максимально качественно так, как указано у нас в примерах, а также снимите объявление с других площадок по продаже авто. Оценка через аукцион приводит к увеличению цены за автомобиль. В 80% случаев оценка выше, чем получится в трейд-ин.',
        isHidden: false,
        color: '#F1F1F1'
    },
    {
        title: 'Как вы оцениваете машину?',
        content:
            'Сделанные вами качественные фотографии автомобиля просматриваются дилерами, автосалонами и профессиональными покупателями которые делают свои ставки и за 25-30 минут определяется максимальная цена, за которую готовы выкупить автомобиль здесь и сейчас.',
        isHidden: false,
        color: '#F1F1F1'
    },
    {
        title: 'Так кому я продаю машину?',
        content:
            'Финальными приобретателями автомобиля являются автосалоны, дилеры и другие профессионалы рынка автомобилей.',
        isHidden: false,
        color: '#F1F1F1'
    },
    {
        title: 'Можно оставить вам автомобиль на продажу?',
        content:
            'Да. Вы можете оставить свой  автомобиль  нашим партнерам, которые его продадут.',
        isHidden: false,
        color: '#F1F1F1'
    },
  ]);
  const {id} = useParams()
const toggle = card => {
    const cards = state;
    const index = cards.indexOf(card);
    setState([
        ...cards.slice(0, index),
        { ...card, isHidden: !card.isHidden },
        ...cards.slice(index + 1),
      ],
    );
  };
  return (
    <div class="container-seller"  style={{'padding-bottom': '30px'}}>
      <Helmet>
        <title>Carfast.kz для продавцов</title>
        <script type='text/javascript'>{`
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '865910560669552');
          fbq('track', 'PageView');`}
        </script>
        <noscript>
          {`
            <img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=865910560669552&ev=PageView&noscript=1"
          />
          `}
        </noscript>
      </Helmet>
      <header className="header">
        <div className="header-wrapper">
          <img className="logo" src={logo} alt="logo" />
          <p>Первый в Казахстане онлайн аукцион автомобилей</p>
        </div>
        <div className="header-content">
          <h1 className="page1-title">
            Покупай и продавай (автомобили) по самой выгодной цене <span>без посредников и без комисий</span>
          </h1>
          <div className="car-image-wrapper">
            <img src={car} alt="car" className="car"></img>
          </div>
        </div>
      </header>
      <section class="page1">
        <ul className="page1-seller-ul">
          <li>8 из 10 автомобилей оцениваются выше, чем в Trade-In</li>
          <li>20 000+ покупателей делают ставки на Ваш автомобиль</li>
          <li>Любое авто, любого года может быть выставлено на аукцион</li>
        </ul>
            
        <a href={id ? ("https://auction.carfast.kz/upload-photos/"+ id) : "https://auction.carfast.kz/upload-photos/"} class="buy-button">ХОЧУ ДОРОГО ПРОДАТЬ АВТО</a>

        <a href="https://auction.carfast.kz/" class="sale-button">ХОЧУ КУПИТЬ АВТО</a>
      </section>
      <section class="page2">
        <h2>КАК ЭТО РАБОТАЕТ?</h2>
        <div className="image-card-wrapper">
          <img src={auctionScreen1} className="image-card" alt="image" />
          <p>Сделайте фото Вашего
          авто по нашим примерам</p>
        </div>
        <div className="image-card-wrapper">
          <img src={auctionScreen2} className="image-card" alt="image" />
          <p>Наблюдайте как цена на Ваше авторастет
и дождитесь окончания аукциона</p>
        </div>
        <div className="image-card-wrapper">
          <img src={auctionScreen3} className="image-card" alt="image" />
          <p>Соглашайтесь на самую высокую
предложенную цену</p>
        </div>

        <a href={id ? ("https://auction.carfast.kz/upload-photos/"+ id) : "https://auction.carfast.kz/upload-photos/"} class="buy-button">ХОЧУ ДОРОГО ПРОДАТЬ АВТО</a>
      </section>
      <section class="page3">
        <h2>ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ</h2>
        <div id='questions'>
          <div className='question-wrapper'>
              {state.map(card => {
                  return (
                      <Card
                      toggleIsHidden={() => toggle(card)}
                      color={card.color}
                      title={card.title}
                      isHidden={card.isHidden}
                      content={card.content}
                      isOpen = {card.isHidden}
                      />
                  );
              })}
          </div>
        </div>

        <a href={id ? ("https://auction.carfast.kz/upload-photos/"+ id) : "https://auction.carfast.kz/upload-photos/"} class="buy-button" style={{ 'margin-top': '40px' }}>ВСЕ ПОНЯТНО, ХОЧУ ПРОДАТЬ АВТО</a>

        <a href="https://auction.carfast.kz/" class="sale-button">ХОЧУ КУПИТЬ АВТО</a>
      </section>
    </div>
  )
}