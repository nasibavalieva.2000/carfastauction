import { useState, useEffect } from 'react'
import { Navigation } from './components/navigation'
import { Header } from './components/header'
import { Features } from './components/features'
import { TradeIn } from './components/tradein'
import { About } from './components/about'
import { Questions } from './components/questions'

import { Team } from './components/Team'
import { Contact } from './components/contact'
import { Partners } from './components/partners'
import JsonData from './data/data.json'
import SmoothScroll from 'smooth-scroll'

import { Seller } from "./components/Seller/Seller"
import { Buyer } from "./components/Buyer/Buyer"
import { Route, Switch, useLocation } from 'react-router-dom'
import ReactGA from 'react-ga';
export const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 1000,
  speedAsDuration: true,
})





// ReactGA.initialize('G-FZC35MZ5T1');
// ReactGA.pageview(window.location.pathname + window.location.search);


const App = () => {
  const [landingPageData, setLandingPageData] = useState({})
  let location = useLocation()
  useEffect(() => {
    if (!window.GA_INITIALIZED) {
      ReactGA.initialize('G-FZC35MZ5T1')
      window.GA_INITIALIZED = true;
    }
    ReactGA.set({ page: location.pathname })
    ReactGA.pageview(location.pathname)
    setLandingPageData(JsonData)
  }, [location])

  return (
    <div>
      <Route exact path="/">
        <Navigation />
        <Header data={landingPageData.Header} />
        <TradeIn data={landingPageData.TradeIn}/>
        <Features data={landingPageData.Features} />
        <About data={landingPageData.About} />
        <Partners data={landingPageData.Partners} />
        <Team data={landingPageData.Team} />
        <Questions data={landingPageData.Questions} />
        <Contact data={landingPageData.Contact} />
      </Route>
      <Route path="/seller/:id?" component={Seller} />
      <Route path="/buyer" component={Buyer} />
    </div>
  )
}

export default App
